import "./styles/app.scss";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { routes } from "./routes";
import { Suspense } from "react";

function App() {
  return (
    <BrowserRouter>
      <Suspense fallback={<p> Loading...</p>}>
        <Routes>
          {routes.map((route, index) => (
            <Route key={index} {...route} />
          ))}
        </Routes>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
