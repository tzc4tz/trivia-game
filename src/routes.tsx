import React from "react";
import RequireUser from "./containers/requireUser";

const Login = React.lazy(() => import("./containers/login"));
const Questions = React.lazy(() => import("./containers/questions"));
const Result = React.lazy(() => import("./containers/result"));

export const routes = [
  {
    path: "/",
    element: <Login />,
  },
  {
    path: "/questions",
    element: (
      <RequireUser>
        <Questions />
      </RequireUser>
    ),
  },
  {
    path: "/result",
    element: (
      <RequireUser checkResult>
        <Result />
      </RequireUser>
    ),
  },
];
