import "./index.scss";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import * as ReactDOMClient from "react-dom/client";

const container = document.getElementById("root");

const root = ReactDOMClient.createRoot(container as HTMLElement);

root.render(<App />);

reportWebVitals();
