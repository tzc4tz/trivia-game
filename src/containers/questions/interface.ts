export interface QuestionType {
  category?: string;
  correct_answer?: string;
  difficulty?: string;
  incorrect_answers?: string[];
  question?: string;
  type?: string;
}

export interface AnswersType {
  title: string | undefined;
  correct: boolean;
  id: number;
}

export type QuestionsType = QuestionType[] | [];
