import { useEffect, useRef, useState } from "react";
import { CountdownCircleTimer } from "react-countdown-circle-timer";
import { useNavigate } from "react-router-dom";
import Answers from "../../components/answers";
import QuestionsBox from "../../components/questionsBox";
import { QuestionsType } from "./interface";
import styles from "./questions.module.scss";

const Questions = () => {
  const finishRef = useRef<null | ReturnType<typeof setTimeout>>(null);
  const stepRef = useRef<null | ReturnType<typeof setTimeout>>(null);
  const timeRef = useRef<null | ReturnType<typeof setTimeout>>(null);
  const navigate = useNavigate();
  const [questions, setQuestions] = useState<QuestionsType>([]);
  const [step, setStep] = useState(0);
  const [pause, setPause] = useState(true);
  const [correctAnswers, setCorrectAnswers] = useState<boolean[] | []>([]);

  useEffect(() => {
    fetchQuestions();
    return () => {
      if (timeRef.current) clearTimeout(timeRef.current);
      if (stepRef.current) clearTimeout(stepRef.current);
      if (finishRef.current) clearTimeout(finishRef.current);
    };
  }, []);

  const fetchQuestions = () => {
    fetch("https://opentdb.com/api.php?amount=10&type=multiple")
      .then((res) => res.json())
      .then((response) => {
        setQuestions(response.results);
        setPause(false);
      })
      .catch(() => {
        sessionStorage.removeItem("username");
        navigate("/");
      });
  };

  const finishedQuestions = () => {
    sessionStorage.setItem("result", JSON.stringify(correctAnswers));
    navigate("/result");
  };

  const answerSelected = (isCorrect: boolean) => {
    setPause(true);
    if (step !== 9) {
      setCorrectAnswers((prev) => {
        return [...prev, isCorrect];
      });

      stepRef.current = setTimeout(() => {
        setPause(false);
        setStep((prev) => prev + 1);
      }, 500);
      return;
    }
    finishRef.current = setTimeout(() => finishedQuestions(), 500);
  };

  return (
    <div className={styles["questions"]}>
      <div className={styles["questions__timer"]}>
        <CountdownCircleTimer
          key={step}
          rotation="counterclockwise"
          trailColor="#ffffff80"
          isPlaying={!pause}
          onComplete={() => {
            if (step !== 9) {
              timeRef.current = setTimeout(
                () => setStep((prev) => prev + 1),
                1000
              );
            } else finishedQuestions();
          }}
          duration={10}
          colors={["#FFF", "#f48889"]}
          colorsTime={[5, 0]}
        >
          {({ remainingTime }) => remainingTime}
        </CountdownCircleTimer>
        <div className={styles["questions__box"]}>
          {questions.length !== 0 && (
            <QuestionsBox questions={questions} step={step} />
          )}
        </div>
      </div>
      <div className={styles["questions__answers"]}>
        {questions.length !== 0 && (
          <Answers
            questions={questions}
            step={step}
            selectAnswer={answerSelected}
          />
        )}
      </div>
    </div>
  );
};

export default Questions;
