import cn from "classnames";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import styles from "./result.module.scss";

const Result = () => {
  const navigate = useNavigate();
  const [data, setData] = useState({ result: [], username: "" });

  useEffect(() => {
    const result = JSON.parse?.(sessionStorage.getItem("result") as string);
    const username = sessionStorage.getItem("username");
    setData({ result, username: username as string });
  }, []);

  const retryGame = () => {
    sessionStorage.removeItem("result");
    navigate("/questions");
  };

  const correct = data?.result.filter((item) => item).length;

  return (
    <div className={cn(styles["result"], "container")}>
      <div className={styles["result__info"]}>
        <div>Great!</div>
        <div>{data?.username}</div>
        <div>You've Answerd</div>
        <div className={styles["result__info--answers"]}>{correct}/10</div>
        <div>of Questions</div>
      </div>
      <div className={styles["result__retry"]}>
        <button onClick={retryGame}>Retry</button>
      </div>
    </div>
  );
};

export default Result;
