import { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";

const RequireUser = ({
  children,
  checkResult,
}: {
  children: JSX.Element;
  checkResult?: boolean;
}) => {
  const [auth, setAuth] = useState(true);
  const [resultAuth, setResultAuth] = useState(true);

  const renderChildren = () => {
    if (auth) {
      if (checkResult) {
        if (resultAuth) return children;
        return <Navigate to="/questions" replace />;
      }
      return children;
    }
    return <Navigate to="/" replace />;
  };

  useEffect(() => {
    const user = sessionStorage.getItem("username");
    const result = sessionStorage.getItem("result");
    if (!user) setAuth(false);
    if (!result && checkResult) setResultAuth(false);
  }, []);

  return renderChildren();
};

export default RequireUser;
