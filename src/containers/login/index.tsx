import cn from "classnames";
import { ChangeEvent, useState } from "react";
import { useNavigate } from "react-router-dom";
import styles from "./login.module.scss";

const Login = () => {
  const navigate = useNavigate();
  const [username, setUsername] = useState("");

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setUsername(value);
  };

  const submitUser = () => {
    navigate("/questions");
    sessionStorage.setItem("username", username);
  };

  return (
    <div className={cn(styles["login"], "container")}>
      <div className={styles["login__title"]}>Hi!</div>
      <div className={styles["login__subtitle"]}>What's your name?</div>
      <div className={styles["login__input--wrapper"]}>
        <input
          placeholder="Your Name"
          value={username}
          onChange={handleChange}
        />
      </div>
      <div className={styles["login__submit"]}>
        <button disabled={!username} onClick={submitUser}>
          Start Game
        </button>
      </div>
    </div>
  );
};

export default Login;
