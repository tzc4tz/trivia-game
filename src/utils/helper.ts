export const parseLiterals = (text: string) =>
  text.replaceAll("&quot;", '"').replaceAll("&#039;", "'");
