import { QuestionsType } from "../../containers/questions/interface";
import { parseLiterals } from "../../utils/helper";
import styles from "./questionsBox.module.scss";

interface Props {
  questions: QuestionsType;
  step: number;
}

const QuestionsBox = ({ questions, step }: Props) => {
  return (
    <div className={styles["questionsBox"]}>
      <div>Question {step + 1}/10</div>
      <div className={styles["questionsBox--question"]}>
        {parseLiterals(questions[step]?.question as string)}
      </div>
    </div>
  );
};

export default QuestionsBox;
