import cn from "classnames";
import { useEffect, useRef, useState } from "react";
import {
  AnswersType,
  QuestionsType,
} from "../../containers/questions/interface";
import { parseLiterals } from "../../utils/helper";
import styles from "./answers.module.scss";

interface Props {
  questions: QuestionsType;
  step: number;
  selectAnswer: (isCorrect: boolean) => void;
}

const Answers = ({ questions, step, selectAnswer }: Props) => {
  const timeRef = useRef<null | ReturnType<typeof setTimeout>>(null);
  const [answers, setAnswers] = useState<AnswersType[] | []>([]);
  const [selectedId, setSelectedId] = useState<number | null>(null);

  const shuffleArray = (array: AnswersType[]) =>
    array.sort(() => Math.random() - 0.5);

  useEffect(() => {
    return () => {
      if (timeRef.current) clearTimeout(timeRef.current);
    };
  }, []);

  useEffect(() => {
    const allAnswers = [];
    allAnswers.push({
      title: questions[step].correct_answer,
      correct: true,
      id: 0,
    });

    questions[step].incorrect_answers?.map((answer, index) =>
      allAnswers.push({ title: answer, correct: false, id: index + 1 })
    );
    setAnswers(shuffleArray(allAnswers));
  }, [step]);

  return (
    <div className={styles["answers"]}>
      {answers?.map((answer, index) => (
        <div
          key={index}
          className={cn(
            styles["answers__box"],
            selectedId === answer.id
              ? answer.correct
                ? styles["answers__box--correct"]
                : styles["answers__box--wrong"]
              : ""
          )}
        >
          <button
            onClick={() => {
              selectAnswer(answer.correct);
              setSelectedId(answer.id);
              timeRef.current = setTimeout(() => setSelectedId(null), 500);
            }}
          >
            {parseLiterals(answer.title as string)}
          </button>
        </div>
      ))}
    </div>
  );
};

export default Answers;
